# vue-css

## Project setup
```
npm install
```

### Compiles and hot-reloads for development

For Android:

```
npm run serve-android
```

For desktop (same machine):

```
npm run serve-desktop
```

You can add the FORCE_SSL var to make it sure the API calls will use https:

```
FORCE_SSL=true npm run serve

```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

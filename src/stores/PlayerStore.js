import { defineStore } from 'pinia'

/**
 * Manages the state of the player
 */
export const usePlayerStore = defineStore('player', {

  state: () => ({
    playing: false,
    nowPlaying: Object,
    playqueueData: Object,
    playqueueShowing: false,
    mediaInfoShowing: false,
    playQueueEdit: false,
    responsivePlayerShowing: false,
    showRemote: false,
    volume:0,
    waitingConfirmation: Object,
  }),
  getters: {
  },
  actions: {
    togglePlayQueueEdit() {
      this.playQueueEdit = !this.playQueueEdit
    },
  },
})
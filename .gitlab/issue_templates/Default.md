<!--- 
/!\ Read this first!/!\  

This default template purpose is to be used to report a bug you found in the remote acccess.

To use a different template, select it from the "Description" drop-down above. If you're having trouble finding it, see https://code.videolan.org/videolan/vlc-android/-/wikis/Create-an-issue-and-use-a-template.

If you want to ask for a new feature, please use the "Remote access - Feature Request.md" template above.

If you just want to ask questions about VLC's remote access, please use our forum at https://forum.videolan.org

Please note that any ticket not using a template may be closed without notice as it won't provide the necessary information.

-->



<!--- Provide a general summary of the issue in the Title above -->

## Description

<!--- Describe your bug in detail -->

#### Expected behavior

#### Actual behavior

#### Steps to reproduce

1.
2.
3.

#### Screenshot / video

<!--Add a screenshot or screencast when applicable-->


## Context

#### Remote access version

<!--You can find it in the About screen-->

#### Port version

<!--Remove the useless ports-->

Android

Windows/Linux

MacOS

iOS
